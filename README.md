# Sherumite Theme

Version of the world famous Sherumite theme for Visual Studio Code.

![Screenshot](https://gitlab.com/gsheru/sherumite-theme-vsc/raw/master/screenshot.png "Sherumite Theme screenshot")

### Features
* Light Theme
* Dark theme (WIP)

